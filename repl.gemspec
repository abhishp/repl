Gem::Specification.new do |repl|
  repl.name          = 'repl'
  repl.version       = '0.0.1'
  repl.date          = '2019-03-13'
  repl.summary       = 'Ruby REPL'
  repl.description   = 'A simple ruby repl to be used in interactive ruby apps.'
  repl.authors       = ['Abhishek Puri']
  repl.email         = 'puri.abhishek14@gmail.com'
  repl.files         = `git ls-files bin lib *.md`.split("\n")
  repl.require_paths = ['lib']
  repl.executables   << 'repl'
  repl.license       = 'MIT'
  repl.required_ruby_version = ">= 2.1"
end
