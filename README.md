# REPL

A simple ruby console for interactive ruby use.

## Features

* Running ruby code.
* Running shell commands from within repl.
* Nested context execution of code.

## Installation

```bash
$ cd <repl-dir>
$ bundle install
$ rake install
```

## Running tests
```bash
$ cd <repl-dir>
$ bundle install
$ rake spec
```

## Future improvements

* add vi edit feature to edit and reload the class
