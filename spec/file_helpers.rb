require 'fileutils'

module SpecFileHelpers
  def tempfile(name, mode = 'w+')
    File.new(spec_file_path(name), mode)
  end

  def remove_tempfile(name)
    FileUtils.rm_f(spec_file_path(name))
  end

  private

  def spec_file_path(name)
    File.join('spec','temp', name)
  end

end

