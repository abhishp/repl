require 'repl/object_ext'
require 'repl/context'
require 'repl/context_stack'

describe Repl::ContextStack do 
  let(:context_with_toplevel_binding) {Repl::Context.new TOPLEVEL_BINDING}
  context 'initialization' do 
    it 'should initialize with the toplevel binding by default' do
      expect(Repl::ContextStack.new.top).to eq context_with_toplevel_binding
    end

    it 'should use the specified binding when provided' do 
      new_top_level_binding = binding
      stack = Repl::ContextStack.new new_top_level_binding

      expect(stack.top).to eq Repl::Context.new new_top_level_binding
    end
  end

  context 'instance methods' do
    subject {Repl::ContextStack.new}

    context '#top' do
      it 'should return the top most binding' do
        expect(subject.top).to eq context_with_toplevel_binding
        expect(subject.instance_variable_get(:@context_stack)).to eq [context_with_toplevel_binding]
      end
    end

    context '#push' do
      it 'should push the binding into a new context object' do 
        target = binding
        subject.push(target)

        expect(subject.top).to eq Repl::Context.new target

        target_2 = binding
        subject.push(target_2)

        expect(subject.top).to eq Repl::Context.new target_2
      end
    end

    context '#pop' do
      it 'should pop and return the top context' do
        expect(subject.pop).to eq context_with_toplevel_binding
      end
      
      it 'should raise an error when stack is empty' do
        subject.pop

        expect{subject.pop}.to raise_error(Repl::ContextStackEmptyError, 'No more context available in stack')
      end
    end

    context '#empty?' do
      it 'should return true when the stack is empty' do
        subject.pop

        expect(subject).to be_empty
      end
      
      it 'should return false when the stack is not empty' do
        expect(subject).to_not be_empty
      end
    end
  end
end
