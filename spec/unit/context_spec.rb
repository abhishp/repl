require 'repl/object_ext'
require 'repl/context'

describe Repl::Context do
  context 'initialization' do
    it 'should initialize the target binding in case binding is specified' do
      target = binding

      ctx = Repl::Context.new(target)

      expect(ctx.send(:target)).to eq target
    end

    it 'should initialize the target binding with the binding for target object' do
      target = Integer

      ctx = Repl::Context.new(target)

      expect(ctx.send(:target).receiver).to eq Integer
    end

    it 'should initialize the line counter with zero' do
      expect(Repl::Context.new(Integer).line_counter).to be_zero
    end

    it 'should raise InvalidTargetError when the target is neither a class nor TOPLEVEL_BINDING' do
      expect { Repl::Context.new({}) }.to raise_error Repl::InvalidTargetError, "Invalid context target of type 'Hash'"
      expect { Repl::Context.new(TOPLEVEL_BINDING) }.to_not raise_error
      expect { Repl::Context.new(Hash) }.to_not raise_error
    end
  end

  context 'instance methods' do
    context '#eval' do
      let(:target) {binding}
      
      subject { Repl::Context.new  target}

      it 'should increment the line counter' do
        expect(subject.line_counter).to be_zero

        subject.eval('a=1')

        expect(subject.line_counter).to eq 1
      end

      it 'should evaluate the command in context of target' do
        subject.eval('a=1')

        expect(target.eval('a')).to eq 1
      end

      it 'should skip evaluation but increment the counter if the command is empty string' do
        expect(subject.line_counter).to be_zero

        expect(target).to_not receive(:send)
      
        subject.eval('')

        expect(subject.line_counter).to eq 1
      end
    end

    context '#receiver_name' do
      it 'should return the target name' do
        expect(Repl::Context.new(TOPLEVEL_BINDING).receiver_name).to eq 'main'
        expect(Repl::Context.new(Integer).receiver_name).to eq 'Integer'
      end
    end

    context '#==' do
      let(:target) {binding}
      subject {Repl::Context.new(target)}

      it 'should return false when non context object is compared' do
        expect(subject).to_not eq 1
        expect(subject).to_not eq 'str'
        expect(subject).to_not eq({})
      end

      it 'should return false when context with diff target is compared' do
        expect(subject).to_not eq Repl::Context.new binding
      end

      it 'should return true when context same target and line counter is compared' do
        ctx = Repl::Context.new target

        expect(subject.line_counter).to eq ctx.line_counter 
        expect(subject).to eq ctx
      end

      it 'should return false when context with diff line number is compared' do
        ctx = Repl::Context.new target
        ctx.eval('')
        
        expect(subject.line_counter).to_not eq ctx.line_counter 
        expect(subject).to_not eq ctx
      end      
    end

    context '#toplevel?' do
      it 'should return true when target is toplevel binding' do
        expect(Repl::Context.new TOPLEVEL_BINDING).to be_toplevel
      end

      it 'should return false when target is not toplevel binding' do
        expect(Repl::Context.new binding).to_not be_toplevel
      end
    end
  end
end