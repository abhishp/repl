require 'repl/config'

describe Repl::Configuration do
  context 'defaults' do
    subject {Repl::Configuration.new({})}

    it 'should return stdin as the input' do
      expect(subject.input).to eql $stdin
    end

    it 'should return stdout as the output' do
      expect(subject.output).to eql $stdout
    end

    it 'should return the default prompt prefix' do
      expect(subject.prompt_prefix).to eq Repl::DEFAULT_PROMPT_PREFIX
    end

    it 'should return the default continuation prompt indent' do
      expect(subject.prompt_continuation_indent).to eq Repl::DEFAULT_CONTINUATION_INDENT
    end

    it 'should return the default commands' do
      expect(subject.commands).to eq({
        system: /\A\s*\.(?<command>.*)\z/, 
        inspect_with_repl: /\A\s*repl\s+(?<target>\b\w+(?:::\w+)*\b)\s*\z/
      })
    end
  end

  context 'specified values' do
    let(:input) {double('Input')}
    let(:output) {double('Output')}
    let(:prompt_prefix) {'Prompt Text'}
    let(:continuation_indent) {'Continuation Indent'}
    let(:commands) {{cmd1: /foo-bar/}}

    it 'should return specified input' do
      config = Repl::Configuration.new input: input

      expect(config.input).to eql input
    end

    it 'should return specified output' do
      config = Repl::Configuration.new output: output

      expect(config.output).to eql output
    end

    it 'should return the prompt with specified prefix' do
      config = Repl::Configuration.new prompt_prefix: prompt_prefix

      expect(config.prompt_prefix).to eq prompt_prefix
    end

    it 'should return the specified continuation prompt indent' do
      config = Repl::Configuration.new prompt_continuation_indent: continuation_indent

      expect(config.prompt_continuation_indent).to eq continuation_indent
    end

    it 'should merge the specified commands with existing ones' do
      config = Repl::Configuration.new commands: commands
      
      expect(config.commands).to eq({
        system: /\A\s*\.(?<command>.*)\z/, 
        inspect_with_repl: /\A\s*repl\s+(?<target>\b\w+(?:::\w+)*\b)\s*\z/,
        cmd1: /foo-bar/
      })
    end

    it 'should override the default commands with specified ones' do
      config = Repl::Configuration.new commands: {system: /override/}
      
      expect(config.commands).to eq({
        system: /override/,
        inspect_with_repl: /\A\s*repl\s+(?<target>\b\w+(?:::\w+)*\b)\s*\z/,
      })
    end
  end
end