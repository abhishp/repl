require 'repl/io/commands/inspect_with_repl'
require 'repl/io/commands/ruby_code'
require 'repl/io/commands/system'
require 'repl/io/parser'

module Repl::IO
  describe Parser do
    let(:commands) {
      {
        system: /\A\s*system (?<command>.*)\z/,
        inspect_with_repl: /\A\s*r\s+(?<target>\b\w+(?:::\w+)*\b)\s*\z/,
      }
    }
    let(:config) {double('Configuration', commands: commands)}

    subject {Parser.new(config)}

    context 'initialization' do
      it 'should initialize commands' do
        expect(subject.instance_variable_get(:@commands)).to eq(commands.invert)
      end
    end

    context 'instance methods' do
      RSpec::Matchers.define :match_data do |matched_groups|
        match do |actual|
          matched_groups.all? do |key, value|
            actual[key] == value
          end
        end
      end

      context '#parse' do
        context 'system' do
          let(:system_command) {double(Repl::IO::Commands::System)}
          let(:command) {'system cmd arg1 arg2'} 

          it 'should parse the system command' do
            expect(Repl::IO::Commands::System).to receive(:new).and_return(system_command)

            expect(subject.parse(command)).to eq system_command
          end

          it 'should send the system command as the dynamic params' do
            dynamic_params = {'command' => 'cmd arg1 arg2'}
            expect(Repl::IO::Commands::System).to receive(:new).once.with(match_data(dynamic_params))

            subject.parse(command)
          end
        end

        context 'ruby code' do
          let(:ruby_code_command) {double(Repl::IO::Commands::RubyCode)}
          let(:ruby_code) {"def foo\n  :bar\nend"}

          it 'should parse the ruby code command' do
            expect(Repl::IO::Commands::RubyCode).to receive(:new).and_return(ruby_code_command)

            expect(subject.parse(ruby_code)).to eq ruby_code_command
          end

          it 'should send the command as the parameter' do
            expect(Repl::IO::Commands::RubyCode).to receive(:new).once.with(ruby_code).and_return(ruby_code_command)

            expect(subject.parse(ruby_code)).to eq ruby_code_command
          end
        end

        context 'inspect with repl' do
          let(:inspect_command) {double(Repl::IO::Commands::InspectWithRepl)}

          it 'should parse the inspect_with_repl command' do
            expect(Repl::IO::Commands::InspectWithRepl).to receive(:new).and_return(inspect_command)

            expect(subject.parse('r Repl')).to eq inspect_command
          end

          it 'should send the target as a dynamic param' do
            dynamic_params = {'target' => 'Repl::IO::Commands'}
            expect(Repl::IO::Commands::InspectWithRepl).to receive(:new).once.with(match_data(dynamic_params))

            subject.parse(' r Repl::IO::Commands')
          end
        end
      end
    end
  end
end
