require 'repl/io/input'
require 'tempfile'

module Repl::IO
  describe Input do
    let(:prompt_prefix) {'prompt'}
    let(:prompt_continuation_indent) {"\t\t"}
  
    context 'initialization' do
      let(:input) {double('Input')}
      let(:output) {double('Output')}
      let(:config) {double('Configuration')}
        
      it 'should initialize the prompt_prefix' do
        expect(config).to receive(:prompt_prefix).once.and_return(prompt_prefix)
        allow(config).to receive(:prompt_continuation_indent).and_return(prompt_continuation_indent)
        allow(config).to receive(:input)
        allow(config).to receive(:output)

        expect(Input.new(config).instance_variable_get(:@prompt_prefix)).to eql prompt_prefix
      end

      it 'should initialize the continuation prompt indent' do
        allow(config).to receive(:prompt_prefix).and_return(prompt_prefix)
        expect(config).to receive(:prompt_continuation_indent).once.and_return(prompt_continuation_indent)
        allow(config).to receive(:input)
        allow(config).to receive(:output)

        expect(Input.new(config).instance_variable_get(:@continuation_indent)).to eq prompt_continuation_indent
      end

      it 'should initialize the input' do
        allow(config).to receive(:prompt_prefix).and_return(prompt_prefix)
        allow(config).to receive(:prompt_continuation_indent).and_return(prompt_continuation_indent)
        expect(config).to receive(:input).once.and_return(input)
        allow(config).to receive(:output)

        expect(Input.new(config).instance_variable_get(:@input)).to eql input
      end

      it 'should initialize the output' do
        allow(config).to receive(:prompt_prefix).and_return(prompt_prefix)
        allow(config).to receive(:prompt_continuation_indent).and_return(prompt_continuation_indent)
        allow(config).to receive(:input)
        expect(config).to receive(:output).once.and_return(output)

        expect(Input.new(config).instance_variable_get(:@output)).to eql output
      end
    end

    context 'instance methods' do
      let(:config) { double('Configuration',
        input: input,
        output: output,
        prompt_prefix: prompt_prefix,
        prompt_continuation_indent: prompt_continuation_indent)
      }

      subject {Input.new config}

      context '#read' do
        let(:input) { tempfile('io-test.input') }
        let(:output) { tempfile('io-test.output') }
      
        let(:command_number) { 10 }
        let(:context_name) { 'main' }

        around(:each) do |example|
          stdin, stdout = $stdin, $stdout
          $stdin = input
          $stdout = output
          
          example.run

          input.close()
          output.close()
          remove_tempfile('io-test.*')
          $stdin, $stdout = stdin, stdout
        end
        
        context 'interactive mode' do
          it 'should display the prompt with command number and context name' do
            subject.read(command_number, context_name)

            output.rewind

            expect(output.read).to eq "[#{command_number}] #{prompt_prefix}(#{context_name})> "
          end

          it 'should return the entered line' do
            input << 'some_input'
            input.rewind

            expect(subject.read(command_number, context_name)).to eq 'some_input'
          end

          it 'should print the continuation prompt' do
            multiline_command = <<-EOM
            def foo
              :bar
            end
            EOM

            input << multiline_command
            input.rewind
            
            subject.read(command_number, context_name)

            output.rewind

            lines = output.readlines
            lines.drop(1).each { |line| expect(line).to start_with("[#{command_number}] prompt(main)* \t\t") }
          end

          it 'should return multiline command' do
            multiline_command = <<-EOM
            def foo
              :bar
            end
            EOM

            input << multiline_command
            input.rewind

            expect(subject.read(command_number, context_name)).to eq multiline_command.chomp
          end

          it 'should return :control_c when interrupt is fired' do
            expect(Readline).to receive(:readline).with("[#{command_number}] prompt(main)> ", true).and_raise(Interrupt)

            expect(subject.read(command_number, context_name)).to eq :control_c
          end

          it 'should return :control_c when ctrl+c is pressed in a multiline input' do
            expect(Readline).to receive(:readline).with("[#{command_number}] prompt(main)> ", true).ordered.and_return('def foo')
            expect(Readline).to receive(:readline).with("[#{command_number}] prompt(main)* \t\t", true).ordered.and_return(':bar')
            expect(Readline).to receive(:readline).with("[#{command_number}] prompt(main)* \t\t", true).ordered.and_raise(Interrupt)

            expect(subject.read(command_number, context_name)).to eq :control_c
          end

          it 'should return :control_d when ctrl+d is pressed' do
            expect(Readline).to receive(:readline).with("[#{command_number}] prompt(main)> ", true).and_return(nil)

            expect(subject.read(command_number, context_name)).to eq :control_d
          end

          it 'should return nil when ctrl+d is pressed in a multiline input' do
            expect(Readline).to receive(:readline).with("[#{command_number}] prompt(main)> ", true).ordered.and_return('def foo')
            expect(Readline).to receive(:readline).with("[#{command_number}] prompt(main)* \t\t", true).ordered.and_return(':bar')
            expect(Readline).to receive(:readline).with("[#{command_number}] prompt(main)* \t\t", true).ordered.and_return(nil)
            
            expect(subject.read(command_number, context_name)).to eq nil
          end

          it 'should allow commands starting with a .' do
            input << '.ls'
            input.rewind

            expect(subject.read(command_number, context_name)).to eq '.ls'
          end
        end
      end
    end
  end
end
