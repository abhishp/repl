require 'repl/io/commands/system'

describe Repl::IO::Commands::System do
  context 'initialization' do
    it 'should initialize the command by extracting it from match data' do
      cmd = Repl::IO::Commands::System.new command: 'cmd arg1 arg2'

      expect(cmd.instance_variable_get(:@cmd)).to eq('cmd arg1 arg2')
    end
  end

  context 'instance methods' do
    context '#process!' do
      let(:config) {double('Configuration', output: StringIO.new)}
      it 'should run the system command' do
        cmd = Repl::IO::Commands::System.new command: 'ls'

        cmd.process!(nil, config)

        config.output.rewind
        res = config.output.read

        expect(res).to include 'Gemfile'
        expect(res).to include 'README.md'
        expect(res).to include 'Rakefile'
        expect(res).to include 'bin'
        expect(res).to include 'lib'
        expect(res).to include 'repl.gemspec'
      end
    end
  end
end
