require 'repl/io/commands/ruby_code'

describe Repl::IO::Commands::RubyCode do
  context 'initialization' do
    it 'should initialize the command' do
      command = Repl::IO::Commands::RubyCode.new 'a=1'

      expect(command.instance_variable_get(:@cmd)).to eq 'a=1'
    end
  end

  context 'instance methods' do
    context '#process!' do
      let(:target) {binding}
      let(:config) {double('Configuration', output: StringIO.new)}

      it 'should process the command in the given context' do
        expect { Repl::IO::Commands::RubyCode.new('a').process!(target, config) }.to raise_error NameError
        config.output.rewind
        expect(config.output.read).to eq ''
        
        Repl::IO::Commands::RubyCode.new('a=1').process!(target, config)
        config.output.rewind
        lines = config.output.readlines
        expect(target.eval('a')).to eq 1
        expect(lines.length).to eq 1
        expect(lines.last).to eq "=> 1\n"
        
        Repl::IO::Commands::RubyCode.new('a').process!(target, config)
        config.output.rewind
        lines = config.output.readlines
        expect(lines.length).to eq 2
        expect(lines.last).to eq "=> 1\n"
      end

      it 'should quote the output based on the type of result' do
        Repl::IO::Commands::RubyCode.new('a=1').process!(target, config)
        Repl::IO::Commands::RubyCode.new("b='1'").process!(target, config)
        Repl::IO::Commands::RubyCode.new('c={a: :b}').process!(target, config)

        config.output.rewind
        lines = config.output.readlines

        expect(lines).to eq ["=> 1\n", "=> \"1\"\n", "=> {:a=>:b}\n"]
      end

      it 'should not output anything when command is empty sting' do
        expect(Repl::IO::Commands::RubyCode.new('').process!(target, config))

        config.output.rewind

        expect(config.output.read).to be_empty
      end

      it 'should not output anything when command is a sting having only whitespace characters' do
        expect(Repl::IO::Commands::RubyCode.new(" \t \n").process!(target, config))

        config.output.rewind

        expect(config.output.read).to be_empty
      end
    end
  end
end