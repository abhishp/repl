require 'repl/io/commands/inspect_with_repl'

describe Repl::IO::Commands::InspectWithRepl do
  context 'initialization' do
    it 'should initialize the command by extracting it from match data' do
      cmd = Repl::IO::Commands::InspectWithRepl.new target: 'Repl::IO'

      expect(cmd.instance_variable_get(:@target)).to eq('Repl::IO')
    end
  end

  context 'instance methods' do
    context '#process!' do
      let(:context) {binding}
      let(:config) {double('Configuration', output: StringIO.new)}
      let(:repl) {double(Repl)}

      it 'should inspect the target with repl' do
        cmd = Repl::IO::Commands::InspectWithRepl.new target: 'Repl'
        expect(Repl).to receive(:current).and_return(repl)
        expect(repl).to receive(:inspect)
        
        cmd.process!(context, config)
      end
    end
  end
end
