require 'repl'

describe Repl do
  let(:context_with_toplevel_binding) {Repl::Context.new TOPLEVEL_BINDING}
  context 'initialization' do
    let(:config) { double(Repl::Configuration) }
    let(:target) { binding }
    
    it 'should initialize the binding stack with the top level binding by default' do
      expect(Repl::ContextStack).to receive(:new).with(TOPLEVEL_BINDING).and_call_original

      repl = Repl.new

      expect(repl.send(:current_context)).to eq context_with_toplevel_binding
    end

    it 'should initialize the configuration with new context name by default' do
      expect(Repl::Configuration).to receive(:new).with({}).and_call_original

      expect(Repl.new.config).to be_instance_of Repl::Configuration
    end
 
    it 'should initialize the input with config by default' do
      allow(Repl::Configuration).to receive(:new).and_return config
      expect(Repl::IO::Input).to receive(:new).with(config)
      allow(Repl::IO::Parser).to receive(:new).with(config)

      Repl.new
    end

    it 'should initialize the binding stack with the target binding' do
      expect(Repl::ContextStack).to receive(:new).with(target).and_call_original

      repl = Repl.new target

      expect(repl.current_context).to eq Repl::Context.new target
    end

    it 'should initialize the config with specified configs' do
      prompt_prefix = 'some prompt'
      expect(Repl::Configuration).to receive(:new).with({prompt_prefix: prompt_prefix}).and_call_original

      repl = Repl.new target, prompt_prefix: prompt_prefix

      expect(repl.config.prompt_prefix).to eq prompt_prefix
    end

    it 'should initialize the parser with config' do
      expect(Repl::Configuration).to receive(:new).and_return config
      expect(Repl::IO::Input).to receive(:new).with(config)
      expect(Repl::IO::Parser).to receive(:new).with(config)

      Repl.new
    end 
  end

  context 'instance methods' do
    let(:output) {tempfile('repl-test.output')}
    let(:input) {tempfile('repl-test.input')}
    let(:parser) {double(Repl::IO::Parser)}
    let(:config) {double(Repl::Configuration, output: output, input: input, commands: {system: /\A\s*\.(?<command>.*)\z/})}
    let(:stack) {double(Repl::ContextStack, top: context_with_toplevel_binding, pop: nil)}
    let(:input) {double(Repl::IO::Input)}
    let(:command) {double('Command')}
    before(:each) do
      allow(Repl::Configuration).to receive(:new).once.and_return(config)
      allow(Repl::ContextStack).to receive(:new).once.and_return(stack)
      allow(Repl::IO::Input).to receive(:new).once.and_return(input)
      allow(Repl::IO::Parser).to receive(:new).once.and_return(parser)
    end

    around(:each) do |example|
      example.run
    rescue SystemExit
    end


    context '#start' do
      it 'should read the command from input with command number' do
        expect(input).to receive(:read).with(1, 'main').and_return(:control_d)
        allow(stack).to receive(:empty?).and_return(true)

        Repl.new.start
      end

      it 'should increment the command number after each command' do
        expect(input).to receive(:read).with(1, 'main').ordered.and_return('a=1')
        expect(parser).to receive(:parse).with('a=1').ordered.and_return(command)
        expect(command).to receive(:process!).with(context_with_toplevel_binding, config) { context_with_toplevel_binding.eval('a=1') }
        expect(input).to receive(:read).with(2, 'main').ordered.and_return('b=12')
        expect(parser).to receive(:parse).with('b=12').ordered.and_return(command) 
        expect(command).to receive(:process!).with(context_with_toplevel_binding, config) { context_with_toplevel_binding.eval('b=12') }
        expect(input).to receive(:read).with(3, 'main').ordered.and_return(:control_d)
        allow(stack).to receive(:empty?).and_return(true)

        Repl.new.start
      end

      it 'should parse the command using parser' do
        expect(input).to receive(:read).with(1, 'main').ordered.and_return('a=1')
        expect(parser).to receive(:parse).with('a=1').ordered.and_return(command)
        expect(command).to receive(:process!).with(context_with_toplevel_binding, config).ordered { context_with_toplevel_binding.eval('a=1') }
        expect(input).to receive(:read).with(2, 'main').ordered.and_return(:control_d)
        allow(stack).to receive(:empty?).and_return(true)

        Repl.new.start
      end

      it 'should print the error class and message with origin when command raises an error' do
        expect(input).to receive(:read).with(1, 'main').ordered.and_return("1/0")
        expect(parser).to receive(:parse).with('1/0').ordered.and_return(command)
        expect(command).to receive(:process!).with(context_with_toplevel_binding, config).ordered { context_with_toplevel_binding.eval('1/0') }
        
        expect(input).to receive(:read).with(2, 'main').ordered.and_return(:control_d)
        allow(stack).to receive(:empty?).and_return(true)

        expect {Repl.new.start}.to raise_error SystemExit

        output.rewind
        expect(output.read).to eq "ZeroDivisionError: divided by 0\nfrom (repl):1:in __repl__\nlogout\n"
      end

      it 'should not increase the command number when ctrl+d is pressed in a multiline command' do
        expect(input).to receive(:read).with(1, 'main').once.ordered.and_return(nil)
        expect(input).to receive(:read).with(1, 'main').once.ordered.and_return(:control_d)
        expect(stack).to receive(:empty?).once.and_return(true)

        Repl.new.start
      end

      it 'should break the loop when the context stack is empty after pressing ctrl+d ' do
        expect(input).to receive(:read).once.ordered.and_return(:control_d)
        expect(stack).to receive(:empty?).once.ordered.and_return(false)
        
        expect(input).to receive(:read).once.ordered.and_return(:control_d)
        expect(stack).to receive(:empty?).once.ordered.and_return(true)
        
        expect {Repl.new.start}.to raise_error SystemExit

        output.rewind
        expect(output.read).to eq "logout\nlogout\n"
      end

      it 'should not increment the count when ctrl+c is pressed and should not output anything' do
        expect(input).to receive(:read).with(1, 'main').once.ordered.and_return(:control_c)
        expect(input).to receive(:read).with(1, 'main').once.ordered.and_return(:control_d)
        allow(stack).to receive(:empty?).and_return(true)

        expect {Repl.new.start}.to raise_error SystemExit

        output.rewind
        lines = output.read.split("\n")

        expect(lines.take(lines.length-1)).to eq ['']
      end
    end

    context '#current_context' do
      let(:stack) {double(Repl::ContextStack)}
      it 'should return the top of the context stack' do
        context = double(Repl::Context)
        expect(stack).to receive(:top).and_return(context)

        expect(Repl.new.current_context).to eql context
      end
    end

    context '#inspect' do
      let(:target) {binding}
      it 'should push the target into the context stack' do
        repl = Repl.new
        expect(stack).to receive(:push).with(target)

        repl.inspect(target)
      end
    end
  end
end