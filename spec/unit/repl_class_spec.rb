require 'repl/repl_class'

describe Repl do
  context 'class methods' do

    context '.init' do
      let(:input) {tempfile('stubbed-io-test.input')}
      let(:output) {tempfile('stubbed-io-test.output')}
    
      around(:each) do |example|
        stdin, stdout = $stdin, $stdout
        $stdin = input
        $stdout = output
        
        example.run

        input.close()
        output.close()
        remove_tempfile('stubbed-io-test.*')
        $stdin, $stdout = stdin, stdout
      end
    
      it 'should raise an error if Readline is not present' do
        expect(Repl).to receive(:require).with('readline').and_raise(LoadError)

        expect { Repl.init }.to raise_error do |error|
          expect(error).to be_instance_of SystemExit
          expect(error.status).to eq 1
        end

        output.rewind
        expect(output.read).to eq <<-EOM
Repl cannot work without Readline please either
  * enable the readline support for ruby
  * reinstall ruby with readline support
        EOM
      end
    end

    context '.current=' do
      it 'should set the given instance on the thread local storage' do
        instance = double(Repl)
        
        Repl.current = instance

        expect(Thread.current[:__repl__]).to eql instance
      end
    end

    context '.current' do
      let(:instance) {double(Repl)}
      before(:each) { Repl.current = nil}

      it 'should get the instance on the thread local storage' do
        Repl.current = instance

        expect(Repl.current).to eql instance
      end

      it 'should initialize a new instance if there is no instance in thread local storage' do
        expect(Repl).to receive(:new).and_return(instance)
        
        expect(Repl.current).to eql instance
        expect(Thread.current[:__repl__]).to eql instance
      end
    end
  end
end
