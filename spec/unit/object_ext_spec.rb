require 'repl/object_ext'

describe Object do
  it 'should add __binding__ method to objects' do
    expect(Object.instance_methods).to include :__binding__
  end

  context '#__binding__' do
    it 'should return the binding context of the oebject' do
      target = {}

      expect(target.__binding__).to be_instance_of Binding
      expect(target.__binding__.receiver).to eql target
    end
  end
end