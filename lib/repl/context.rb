class Repl
  class InvalidTargetError < StandardError
    def initialize(target)
      super("Invalid context target of type '#{target.class}'")
    end
  end

  class Context
    attr_reader :line_counter

    def initialize(target)
      @target = binding_for(target)
      @line_counter = 0
    end

    def eval(command)
      @line_counter += 1
      @target.send(:eval, command) unless command.empty?
    end

    def receiver_name
      @target.receiver.to_s
    end

    def ==(other)
      return false unless Context === other

      self.target == other.target && 
        self.line_counter == other.line_counter
    end

    def toplevel?
      @target == TOPLEVEL_BINDING
    end

    protected

    attr_reader :target

    def binding_for(target)
      return target if Binding === target
      
      raise InvalidTargetError.new(target) unless (TOPLEVEL_BINDING == target || Class == target.class)

      target.__binding__
    end
  end
end
