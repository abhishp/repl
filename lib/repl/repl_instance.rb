
class Repl
  attr_reader :config

  def initialize(target = TOPLEVEL_BINDING, config = {})
    @context_stack = ContextStack.new target
    @config = Configuration.new config
    @input = IO::Input.new @config
    @parser = IO::Parser.new @config
    self.class.current = self
  end

  def start
    loop do
      line_counter = current_context.line_counter + 1
      cmd = @input.read(line_counter, current_context.receiver_name)
      case cmd
      when :control_c
        @config.output.puts ''
      when :control_d
        @config.output.puts "logout"
        @context_stack.pop
        exit(0) if @context_stack.empty?
      when nil
        @config.output.puts ''
      else
        handle_command cmd, line_counter
      end
    end
  end

  def current_context
    @context_stack.top
  end

  def inspect(target)
    @context_stack.push(target)
  end

  private

  def handle_command(cmd, cmd_number)
    command = @parser.parse(cmd)
    command.process!(current_context, @config)
  rescue StandardError => e
    @config.output.puts e.class.to_s + ": " + e.message +
                          "\nfrom (repl):#{cmd_number}:in " +
                          (current_context.toplevel? ? '__repl__' : e.backtrace.join("\n"))
  end
end
