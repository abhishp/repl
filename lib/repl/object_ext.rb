class Object
  def __binding__
    return  class_eval('binding') if is_a? Module
    binding
  end
end

