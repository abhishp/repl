class Repl
  class << self
    def init
      check_readline_compatibility
    end

    def check_readline_compatibility
      require 'readline'
      ::Readline
    rescue LoadError
      puts <<-EOM
Repl cannot work without Readline please either
  * enable the readline support for ruby
  * reinstall ruby with readline support
      EOM
      exit 1
    end

    def current=(instance)
      Thread.current[:__repl__] = instance
    end

    def current
      Thread.current[:__repl__] ||= Repl.new
    end

    private :check_readline_compatibility
  end
end