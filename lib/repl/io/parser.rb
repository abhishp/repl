class Repl
  module IO
    class Parser
      COMMANDS_MAP = {
        system: Commands::System,
        ruby_code: Commands::RubyCode,
        inspect_with_repl: Commands::InspectWithRepl
      }
      def initialize(config)
        @commands = config.commands.invert
        @commands_list = @commands.keys
      end

      def parse(command)
        cmd, cmd_args = @commands_list.reduce([]) do |command_options, com|
          cmd_args = command.match(com)
          command_options.push(@commands[com], cmd_args) unless cmd_args.nil?
          command_options
        end
        cmd, cmd_args = :ruby_code, command if cmd.nil?
        COMMANDS_MAP[cmd].new(cmd_args)
      end
    end
  end
end
