class Repl
  module IO
    module Commands
      class InspectWithRepl
        def initialize(args)
          @target = args[:target]
        end

        def process!(context, config)
          Repl.current.inspect(context.eval(@target))
        end
      end
    end
  end
end
