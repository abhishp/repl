class Repl
  module IO
    module Commands
      class System
        def initialize(args)
          @cmd = args[:command]
        end

        def process!(_, config)
          config.output.puts `#{@cmd}`
        end
      end
    end
  end
end
