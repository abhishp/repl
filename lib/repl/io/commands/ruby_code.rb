class Repl
  module IO
    module Commands
      class RubyCode
        def initialize(command)
          @cmd = command
        end

        def process!(context, config)
          res = context.eval(@cmd).inspect
          config.output.puts "=> #{res}" unless @cmd.match(/\A\s*\z/)
        end
      end
    end
  end
end
