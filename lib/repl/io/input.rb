require 'readline'
require_relative '../config'

class Repl
  module IO
    class Input
      def initialize(config)
        @prompt_prefix = config.prompt_prefix
        @continuation_indent = config.prompt_continuation_indent
        @input = config.input
        @output = config.output
      end

      def read(command_number, context_name)
        handle_readline! command_number, context_name
      rescue Interrupt
        return :control_c
      end

      private 

      def expression_complete?(line)
        catch(:valid) { eval("BEGIN{throw :valid}\n#{line}") } unless line.start_with? '.'
        line !~ /[,\\]\s*\z/
      rescue SyntaxError
        false
      end

      def handle_readline!(number, context_name)
        Readline.input = @input
        Readline.output = @output
        line = Readline.readline input_prompt(number, context_name), true
        return :control_d if line.nil?
        until expression_complete? line
          val = Readline.readline input_continuation_prompt(number, context_name), true
          return if val.nil?
          line += "\n" + val
        end
        line
      end

      def input_prompt(command_number, context_name)
        "[#{command_number}] #{@prompt_prefix}(#{context_name})> "
      end

      def input_continuation_prompt(command_number, context_name)
        input_prompt(command_number,context_name).sub('> ', "* #{@continuation_indent}")
      end
    end
  end
end
