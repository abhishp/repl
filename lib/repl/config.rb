class Repl
  DEFAULT_PROMPT_PREFIX = 'repl'
  DEFAULT_CONTINUATION_INDENT = "\t"
  DEFAULT_COMMANDS = {
    system: /\A\s*\.(?<command>.*)\z/,
    inspect_with_repl: /\A\s*repl\s+(?<target>\b\w+(?:::\w+)*\b)\s*\z/
  }

  class Configuration
    def initialize(configs)
      commands = DEFAULT_COMMANDS.merge(configs.delete(:commands) || {})
      @configs = configs
      @configs[:commands] = commands
    end

    def input
      @configs[:input] || $stdin
    end

    def output
      @configs[:output] || $stdout
    end

    def commands
      @configs[:commands]
    end
    
    def prompt_prefix
      @configs[:prompt_prefix] || Repl::DEFAULT_PROMPT_PREFIX
    end

    def prompt_continuation_indent
      @configs[:prompt_continuation_indent] || Repl::DEFAULT_CONTINUATION_INDENT
    end

    def to_h
      @configs
    end
  end
end
