class Repl
  class ContextStackEmptyError < StandardError
    def initialize
      super('No more context available in stack')
    end
  end

  class ContextStack
    def initialize(global_binding = TOPLEVEL_BINDING)
      @context_stack = []
      push(global_binding)
      @top_context = top
    end

    def top
      @context_stack.last
    end

    def push(target)
      @context_stack.push Context.new target
    end

    def empty?
      @context_stack.empty?
    end

    def pop
      raise ContextStackEmptyError.new if @context_stack.empty?

      @context_stack.pop
    end
  end
end
