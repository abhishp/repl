require_relative './repl/object_ext'

require_relative './repl/config'
require_relative './repl/context'
require_relative './repl/context_stack'

require_relative './repl/io/commands/inspect_with_repl'
require_relative './repl/io/commands/ruby_code'
require_relative './repl/io/commands/system'

require_relative './repl/io/input'
require_relative './repl/io/parser'

require_relative './repl/repl_class'
require_relative './repl/repl_instance'

Repl.init